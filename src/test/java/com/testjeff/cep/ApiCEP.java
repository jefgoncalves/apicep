package com.testjeff.cep;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import io.restassured.RestAssured;
import static io.restassured.RestAssured.*;
import io.restassured.response.ResponseBodyExtractionOptions;
import static org.hamcrest.Matchers.*;

/**
 * @author jefgoncalves
 */
public class ApiCEP {

	private String CEP, state, city, streetFirtName,streetSecordName;

	public ApiCEP() {
		state = new String();
		city = new String();
		streetFirtName = new String();
		streetSecordName = new String();
	}

	public void testInexistentZipCode(String CEP) {
		String inexistentZipCode = CEP;
		RestAssured.baseURI = "https://viacep.com.br/ws/" + inexistentZipCode + "/json/";
		given().contentType("application/json")
		.when()
			.get("/")
		.then()
			.statusCode(200)
			.body("erro", is(true));

		System.out.println("---------------------------------------");
		System.out.println("TestInexistentZipCode");
		System.out.println("CEP: " + inexistentZipCode);
		System.err.println("Erro - CEP Inexistente");
		System.out.println("---------------------------------------");
	}

	public void testInexistentZipCode() {
		String inexistentZipCode = "00310140";
		RestAssured.baseURI = "https://viacep.com.br/ws/" + inexistentZipCode + "/json/";
		given().contentType("application/json")
			.when()
				.get("/")
			.then()
				.statusCode(200)
				.body("erro", is(true));

		System.out.println("---------------------------------------");
		System.out.println("TestInexistentZipCode");
		System.out.println("CEP: " + inexistentZipCode);
		System.err.println("Erro - CEP Inexistente");
		System.out.println("---------------------------------------");

	}

	public void testCorrectZipCode(String CEP) {
		String zipCode = CEP;
		RestAssured.baseURI = "https://viacep.com.br/ws/" + zipCode + "/json/";

		given().contentType("application/json")
			.when()
				.get("/")
			.then()
				.statusCode(200)
				.body("cep", is(zipCode));

		ResponseBodyExtractionOptions body = given().contentType("application/json")
				.when()
					.get("/")
				.then()
					.statusCode(200)
					.body("cep", is(zipCode))
					.extract()
					.body();

		System.out.println("---------------------------------------");
		System.out.println("TestCorrectZipCode");
		System.out.println("Dados Encontrados");
		System.out.println("CEP: " + zipCode);
		System.out.println("logradouro " + body.path("logradouro"));
		// System.out.println("Complemento " + body.path("complemento"));
		System.out.println("Bairro " + body.path("bairro"));
		System.out.println("Localidade " + body.path("localidade"));
		System.out.println("UF " + body.path("uf"));
		System.out.println("IBGE: " + body.path("ibge"));
		System.out.println("---------------------------------------");
	}

	public void testCorrectZipCode() {
		String zipCode = "94834-350";
		RestAssured.baseURI = "https://viacep.com.br/ws/" + zipCode + "/json/";
		given().contentType("application/json")
			.when()
				.get("/")
			.then()
				.statusCode(200)
				.body("cep", is(zipCode));

		ResponseBodyExtractionOptions body = given().contentType("application/json").when().get("/").then()
				.statusCode(200).body("cep", is(zipCode)).extract().body();

		System.out.println("---------------------------------------");
		System.out.println("TestCorrectZipCode");
		System.out.println("Dados Encontrados");
		System.out.println("CEP: " + zipCode);
		System.out.println("logradouro: " + body.path("logradouro"));
		// System.out.println("Complemento " + body.path("complemento"));
		System.out.println("Bairro: " + body.path("bairro"));
		System.out.println("Localidade: " + body.path("localidade"));
		System.out.println("UF: " + body.path("uf"));
		System.out.println("IBGE: " + body.path("ibge"));
		System.out.println("---------------------------------------");
	}

	public void testInvalidZipCode(String CEP) {
		String InvalidZipCode = CEP;
		RestAssured.baseURI = "https://viacep.com.br/ws/" + InvalidZipCode + "/json/";
		given().contentType("application/json")
			.when()
				.get("/")
			.then()
				.statusCode(400);

		System.out.println("---------------------------------------");
		System.out.println("TestInvalidZipCode");
		System.out.println("CEP: " + InvalidZipCode);
		System.err.println("Erro - CEP Inválido");
		System.out.println("---------------------------------------");
	}

	public void testInvalidZipCode() {
		String InvalidZipCode = "9483435";
		RestAssured.baseURI = "https://viacep.com.br/ws/" + InvalidZipCode + "/json/";
		given().contentType("application/json")
		.when()
			.get("/")
		.then()
			.statusCode(404);

		System.out.println("---------------------------------------");
		System.out.println("TestInvalidZipCode");
		System.out.println("CEP: " + InvalidZipCode);
		System.err.println("Erro - CEP Inválido");
		System.out.println("---------------------------------------");
	}

	public void testAddressSearchSimpleNameCEP(String state, String city, String streetFirtName) {
		 this.state = state;
		 this.city = city;
		 this.streetFirtName=streetFirtName;

		RestAssured.baseURI = "https://viacep.com.br/ws/";
		System.out.println(RestAssured.baseURI);
		given().contentType("application/json")
			.when()
				.get(state + "/" + city + "/" + streetFirtName + "/" + "json" + "/")
			.then()
				.statusCode(200);
		
		String json = get(state + "/" + city + "/" + streetFirtName + "/" + "json" + "/").asString();

		if (json.contains(streetFirtName) == true) {
			ResponseBodyExtractionOptions body = given().contentType("application/json")
				.when()
					.get(state + "/" + city + "/" + streetFirtName + "/" + "json" + "/")
					
				.then().statusCode(200).extract()
					.body();

			System.out.println("---------------------------------------");
			System.out.println("TestCorrectZipCode");
			System.out.println("Dados Encontrados");
			System.out.println("Dados Utilizado para Pesquisa:");
			System.out.println("Estado: " + state + ", Cidade: " + city + ", Logradouros que tenha o nome: " + streetFirtName);
			System.out.println("CEP: " + body.path("cep"));
			System.out.println("logradouro " + body.path("logradouro"));
			System.out.println("Bairro " + body.path("bairro"));
			System.out.println("Localidade " + body.path("localidade"));
			System.out.println("UF " + body.path("uf"));
			System.out.println("IBGE: " + body.path("ibge"));
			System.out.println("---------------------------------------");
		} else {
			System.out.println("Endereço Invalido ou endereço inexistente");
		};

	}


	public void testAddressSearchCompoundNameCEP(String state, String city, String streetFirtName,  String Name) {
		 this.state = state;
		 this.city = city;
		 this.streetFirtName=streetFirtName;
		 this.streetSecordName = streetSecordName;

		RestAssured.baseURI = "https://viacep.com.br/ws/";
		given().contentType("application/json")
			.when()
				.get(state + "/" + city + "/" + streetFirtName +"+"+streetSecordName+ "/" + "json" + "/")
			.then()
				.statusCode(200);
		
		String json = get(state + "/" + city + "/" + streetFirtName +"+"+streetSecordName+ "/" + "json" + "/").asString();

		if (json.contains(streetFirtName) == true || json.contains(streetSecordName) == true) {
			ResponseBodyExtractionOptions body = given().contentType("application/json")
				.when()
					.get(state + "/" + city + "/" + streetFirtName +"+"+streetSecordName+ "/" + "json" + "/")
				.then().statusCode(200)
					.extract()
					.body();

			System.out.println("---------------------------------------");
			System.out.println("TestCorrectZipCode");
			System.out.println("Dados Encontrados");
			System.out.println("Dados Utilizado para Pesquisa:");
			System.out.println("Estado: " + state + ", Cidade: " + city + ", Logradouros que tenha os nomes: " + streetFirtName +" ou "+ streetSecordName);
			System.out.println("CEP: " + body.path("cep"));
			System.out.println("logradouro " + body.path("logradouro"));
			System.out.println("Bairro " + body.path("bairro"));
			System.out.println("Localidade " + body.path("localidade"));
			System.out.println("UF " + body.path("uf"));
			System.out.println("IBGE: " + body.path("ibge"));
			System.out.println("---------------------------------------");
		} else {
			System.out.println("Endereço Invalido ou endereço inexistente");
		};

	}

}
