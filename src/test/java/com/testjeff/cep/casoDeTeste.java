package com.testjeff.cep;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.thedeanda.lorem.*;


/**
 *
 * @author jefgoncalves
 */
public class casoDeTeste {
	private ApiCEP CEP;
	private String inexistentZipCode;
	private String invalidZipCode;
	private String corretZipCode;
	private String state;
	private String city;
	private String streetFirtName;
	private String streetSecordName;
	private String streetCompoundName;
	private LoremIpsum lorem;
	
	

	public casoDeTeste() {
		CEP = new ApiCEP();
		inexistentZipCode = new String();
		invalidZipCode = new String();
		corretZipCode = new String();
		state = new String();
		city = new String();
		streetFirtName = new String();
		streetSecordName = new String();
		streetCompoundName = new String();
		lorem = new LoremIpsum();

	}

	//Busca por CEP inexistente
	@Test
	public void testInexistentZipCode() {
		CEP.testInexistentZipCode(inexistentZipCode);
	}

	//Busca por CEP invalido
	@Test
	public void testInvalidZipCode() {
		CEP.testInvalidZipCode(invalidZipCode);
	}

	//Busca por CEP
	@Test
	public void testCorretZipCode1() {
		CEP.testCorrectZipCode(corretZipCode);
	}

	//Busca por CEP
	@Test
	public void testCorretZipCode2() {
		CEP.testCorrectZipCode(corretZipCode);
	}

	// Utilizar nome de rua composto
	@Test
	public void testSearchCorrectAddressCEP1() {
		CEP.testAddressSearchSimpleNameCEP(state, city, streetCompoundName);
	}

	// Utilizar nome de rua simples
	@Test
	public void testAddressSearchSimpleNameCEP() {
		CEP.testAddressSearchSimpleNameCEP(state, city, streetFirtName);
	}
	
	//Busca por endereço inexistente
	@Test
	public void testNameSearchInvalidSimplesNameSearchCEP() {
		CEP.testAddressSearchSimpleNameCEP(state, city, streetFirtName);
	}

	
	// Utilizar nome de rua nao importando a ordem dos nomes colocado para busca, exemplo: Domingos José ou José Domingos
	@Test
	public void testAddressSearchCompoundNameCEP() {
		CEP.testAddressSearchCompoundNameCEP(state,city,streetFirtName,streetSecordName);
	}
	
	//Busca por endereço inexistente
	@Test
	public void testNameSearchInvalidCompoundNameSearchCEP(){
		CEP.testAddressSearchCompoundNameCEP(state,city,streetFirtName,streetSecordName);
	}
	

}
